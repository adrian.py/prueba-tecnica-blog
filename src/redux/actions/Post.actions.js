import { SET_POSTS, RESET_POSTS } from '../types/Post';

//POSTS
export const setPosts = (posts) => {
    return {
        type: SET_POSTS,
        payload: posts
    };
}

export const resetPosts = () => {
    return {
        type: RESET_POSTS
    };
}