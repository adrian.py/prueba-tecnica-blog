import { SET_USUARIO, RESET_USUARIO } from '../types/Usuario';
import { SET_ESTADOS, RESET_ESTADOS} from '../types/Estado';

//Usuarios
export const setUsuario = (usuario) => {
    return {
        type: SET_USUARIO,
        payload: usuario
    };
}

export const resetUsuario = () => {
    return {
        type: RESET_USUARIO
    };
}

//Estados

export const setEstados = (estados) => {
    return {
        type: SET_ESTADOS,
        payload: estados
    };
}

export const resetEstados = () => {
    return {
        type: RESET_ESTADOS
    };
}