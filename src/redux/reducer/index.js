import { combineReducers } from "redux";

import General from './General';
import Post from './Post';

const rootReducer = combineReducers({
    General,
    Post
});

export default rootReducer;