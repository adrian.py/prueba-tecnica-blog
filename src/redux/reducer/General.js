import { SET_USUARIO, RESET_USUARIO } from '../types/Usuario';
import { SET_ESTADOS, RESET_ESTADOS } from '../types/Estado';

const InitialState = {
    usuario: { id: 1, user: 'leo', email: 'adrian129951@gmail.com', nombre: 'Leobardo Herrera' },
    estados: [{id: 1, nombre: "Puebla"}]
};

export default function general(state = InitialState, action) {
    switch (action.type) {
        //USUARIO
        case SET_USUARIO:
            return {
                ...state,
                usuario: action.payload
            }
        case RESET_USUARIO:
            return {
                ...state,
                usuario: { id: 1, user: 'leo', email: 'adrian129951@gmail.com', nombre: 'Leobardo Herrera' }
            }
        //ESTADOS
        case SET_ESTADOS:
            return {
                ...state,
                estados: action.payload
            }
        case RESET_ESTADOS:
            return {
                ...state,
                estados: [{id: 1, nombre: "Puebla"}]
            }
        default:
            return state;
    }
}