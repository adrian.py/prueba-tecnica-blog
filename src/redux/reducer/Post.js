import { SET_POSTS, RESET_POSTS } from '../types/Post';

const InitialState = {
    posts: [
        {
            id: 1,
            estado: 'Puebla',
            nombre: 'Atlixco',
            image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
            rate: 4,
            visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
            hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
            lugares: [
                {
                    nombre: "Escalera ancha",
                    descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                },
                {
                    nombre: "El tamalito",
                    descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                },
                {
                    nombre: "La Pasadita",
                    descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                }
            ],
            date: 'Nov 14'
        },
        {
            id: 2,
            estado: 'Puebla',
            nombre: 'Atlixco',
            image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
            rate: 4,
            visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
            hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
            lugares: [
                {
                    nombre: "Escalera ancha",
                    descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                },
                {
                    nombre: "El tamalito",
                    descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                },
                {
                    nombre: "La Pasadita",
                    descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                }
            ],
            date: 'Nov 14'
        },
        {
            id: 3,
            estado: 'Puebla',
            nombre: 'Atlixco',
            image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
            rate: 4,
            visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
            hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
            lugares: [
                {
                    nombre: "Escalera ancha",
                    descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                },
                {
                    nombre: "El tamalito",
                    descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                },
                {
                    nombre: "La Pasadita",
                    descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                }
            ],
            date: 'Nov 14'
        },
        {
            id: 4,
            estado: 'Puebla',
            nombre: 'Atlixco',
            image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
            rate: 4,
            visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
            hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
            lugares: [
                {
                    nombre: "Escalera ancha",
                    descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                },
                {
                    nombre: "El tamalito",
                    descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                },
                {
                    nombre: "La Pasadita",
                    descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                    image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                }
            ],
            date: 'Nov 14'
        },
    ]
};

export default function post(state = InitialState, action) {
    switch (action.type) {
        //POSTS
        case SET_POSTS:
            return {
                ...state,
                posts: action.payload
            }
        case RESET_POSTS:
            return {
                ...state,
                posts: [
                    {
                        id: 1,
                        estado: 'Puebla',
                        nombre: 'Atlixco',
                        image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
                        rate: 4,
                        visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
                        hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
                        lugares: [
                            {
                                nombre: "Escalera ancha",
                                descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                            },
                            {
                                nombre: "El tamalito",
                                descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                            },
                            {
                                nombre: "La Pasadita",
                                descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                            }
                        ],
                        date: 'Nov 14'
                    },
                    {
                        id: 2,
                        estado: 'Puebla',
                        nombre: 'Atlixco',
                        image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
                        rate: 4,
                        visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
                        hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
                        lugares: [
                            {
                                nombre: "Escalera ancha",
                                descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                            },
                            {
                                nombre: "El tamalito",
                                descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                            },
                            {
                                nombre: "La Pasadita",
                                descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                            }
                        ],
                        date: 'Nov 14'
                    },
                    {
                        id: 3,
                        estado: 'Puebla',
                        nombre: 'Atlixco',
                        image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
                        rate: 4,
                        visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
                        hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
                        lugares: [
                            {
                                nombre: "Escalera ancha",
                                descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                            },
                            {
                                nombre: "El tamalito",
                                descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                            },
                            {
                                nombre: "La Pasadita",
                                descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                            }
                        ],
                        date: 'Nov 14'
                    },
                    {
                        id: 4,
                        estado: 'Puebla',
                        nombre: 'Atlixco',
                        image: "https://mexicorelocationguide.com/wp-content/uploads/2021/11/atlixco-volcano.jpg",
                        rate: 4,
                        visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
                        hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
                        lugares: [
                            {
                                nombre: "Escalera ancha",
                                descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
                            },
                            {
                                nombre: "El tamalito",
                                descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
                            },
                            {
                                nombre: "La Pasadita",
                                descripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                                image: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
                            }
                        ],
                        date: 'Nov 14'
                    },
                ]
            }
        default:
            return state;
    }
}