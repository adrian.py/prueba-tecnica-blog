import React from 'react';
import Home from './components/Home';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Footer from './components/layouts/Footer';
import Header from './components/layouts/header/Header';
import Listado from './components/Listado';
import Post from './components/Post';

function App() {

  const theme = createTheme();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Pueblos Mágicos" />
        <main>

          {/* Rutas */}
          <BrowserRouter>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/listado/:estado" element={<Listado />} />
              <Route exact path="/post/:id" element={<Post />} />
            </Routes>
          </BrowserRouter>
          {/* /Rutas */}

        </main>
        <Footer
          title="Leobardo Adrián Herrera Gálvez"
          description="adrian129951@gmail.com" />

      </Container>
    </ThemeProvider>
  );
}

export default App;