export const pueblos = [
    {
        id: 1,
        imagen: "https://www.viajefest.com/wp-content/uploads/2018/12/Atlixco-que-hacer-y-que-ver-en-la-villa-iluminada.jpeg",
        nombre: 'Atlixco',
        estado: 'Puebla',
        rate: 4,
        visitar: "Atlixco es un lugar mágico que a cada paso que das te va enamorando con su arquitectura, jardines y la calidez de su gente. Caminar por sus calles, tomar un café en su kiosco o disfrutar de un atardecer o amanecer desde el Cerro de San Miguel son algunas de las actividades que puedes hacer cuando lo visites. También destaca por su gran cultivo de flores y cuenta con más de 80 viveros en los que puedes pasar horas admirando las plantas y los campos. Se considera que Atlixco tiene un clima muy similar al de Cuernavaca, de hecho si los comparas en el mapa, te darás cuenta que comparten la misma franja en el territorio de México, no es casualidad que muchas personas jubiladas escojan Atlixco para vivir, y que actualmente haya un boom inmobiliario de casas de fin de semana en éste hermoso municipio. Y si te gustan las fiestas tradicionales no te puedes perder una de sus fiestas más grandes que es el Huey Atlixcáyotl que se celebra el último domingo de septiembre en el Cerro de San Miguel.",
        hacer: "Atlixco ofrece una gran variedad de actividades turísticas a sus visitantes durante todo el año, y eventos culturales de mucho interés en algunas temporadas, siendo los más importantes la Villa Iluminada durante noviembre y diciembre y el festival Huey Atlixcáyotl en septiembre. Aquí está una lista de las actividades más destacadas que se pueden realizar en éste pueblo mágico, dale click a las que más te interesen para continuar leyendo detalles de cada una.",
        lugares: [
            {
                nombre: "Escalera ancha",
                descripcion: "Uno de los rincones más icónicos del pueblo fue renovado con un colorido mural que representa la herencia y tradición cultural de Atlixco. Un lugar que no te debes perder en tu visita.",
                imagen: "https://www.viajefest.com/wp-content/uploads/2020/10/mural-en-la-escalera-ancha-en-atlixco-puebla.png"
            },
            {
                nombre: "El tamalito",
                descripcion: "Es uno de los mejores restaurantes de Atlixco debido a su fina cocina tradicional, que fusiona los sabores prehispánicos con los platillos coloniales.",
                imagen: "https://www.viajefest.com/wp-content/uploads/2020/10/Interior-de-El-Tamalito-en-Atlixco-Puebla.png"
            },
            {
                nombre: "La Pasadita",
                decripcion: "La Pasadita es un espacio creado por Atlixtour para artesanos, productores y artistas de Atlixco y así puedas llevarte un bonito recuerdo de Atlixco. Aquí podrás comprar: Textiles Joyería Artesanal Mezcal Cerveza Artesanal Chocolate Souvenirs Fibras Vegetales Cerámica y alfarería Está muy bien ubicada",
                imagen: "https://www.viajefest.com/wp-content/uploads/2020/09/la-pasadita.jpg"
            }
        ]
    }
];

export const estados = [
    { id: 1, nombre: "Puebla" }
];