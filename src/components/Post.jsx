import React, { useEffect, useState } from 'react';
import Banner from './layouts/Banner';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import Lugar from './layouts/Lugar';

const Post = () => {

    const { posts } = useSelector(state => state.Post);
    const { id } = useParams();
    const [post, setPost] = useState({});
    
    useEffect(() => {
        const findPost = () => {
            const search = posts.filter(p => p.id == id);
            if (search.length > 0) {
                setPost(search[0]);
            }
        }
        findPost();
    }, []);

    return (
        <React.Fragment>
            <Banner title={`${post.nombre}, ${post.estado}`} size={"h4"} image={post.image} style={{ marginBottom: '2rem' }} />
            <Typography variant="h4" gutterBottom >
                ¿Por qué visitar {post.nombre}?
            </Typography>
            <Typography variant="h6" gutterBottom style={{ marginTop: '2rem' }}>
                {post.visitar}
            </Typography>
            <Typography variant="h4" gutterBottom style={{ marginTop: '2rem' }}>
                ¿Qué hacer en {post.nombre}?
            </Typography>
            <Typography variant="h6" gutterBottom style={{ marginTop: '2rem' }}>
                {post.hacer}
            </Typography>

            <div className="grid grid-cols-4 gap-4">
                {post.lugares &&
                    post.lugares.map((l) => (
                        <div key={l.nombre}>
                            <Lugar lugar={l} />
                        </div>
                    ))}
            </div>


        </React.Fragment>
    );
}

export default Post;