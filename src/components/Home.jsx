import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import CardPost from './layouts/CardPost';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import Banner from './layouts/Banner';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Home = () => {
  const { posts } = useSelector(state => state.Post);
  const [lastPosts, setLastPosts] = useState([]);

  useEffect(() => {
    const getLastPosts = () => {
      if (posts) {
        if (posts.length >= 6) {
          setLastPosts(posts.slice(-3));
        } else if (posts.length >= 3) {
          setLastPosts(posts.slice(-2));
        }
      }
    }
    const timeId = setTimeout(() => {
      getLastPosts()
    }, 1000)
    return () => {
      clearTimeout(timeId)
    }
  }, [posts]);

  return (
    <React.Fragment>
      <Banner image={'https://ungustito.mx/wp-content/uploads/2021/03/pueblos-magicos-mexico-2021-requisitos.jpg'}/>
      <div className="flex" style={{ justifyContent: 'flex-end', marginBottom: '1rem' }}>
        <Button className="flex-end" variant="contained">Agregar Post</Button>
      </div>
      <div className="row">
        <Typography variant="h4" gutterBottom>
          Últimos Post
        </Typography>
      </div>
      {lastPosts.length > 0 ?
        <Grid container spacing={4}>
          {lastPosts.map((post) => (
            <CardPost key={post.id} post={post} />
          ))}
        </Grid>
        :
        <div className="grid grid-cols-4 gap-4">
          <div></div>
          <div></div>
          <div>
            <Box sx={{ display: 'flex' }}>
              <CircularProgress />
            </Box>
          </div>
        </div>
      }
    </React.Fragment>
  );
}
export default Home;