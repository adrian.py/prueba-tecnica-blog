import React, { useState } from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useDispatch, useSelector } from 'react-redux';
import { setEstados } from '../../../redux/actions/General.actions';

const AddEstado = ({ open, setOpen, handleClose, setMessageAlert }) => {

    const [newEstado, setNewEstado] = useState({ id: null, nombre: null });
    const dispatch = useDispatch();
    const { estados } = useSelector(state => state.General);

    const saveOnRedux = () => {
        dispatch(setEstados([...estados, newEstado]));
        setMessageAlert("Estado guardado correctamente!");
        setOpen(false);
    };

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description">
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
                    <AddCircleOutlineIcon />Agregar nuevo Estado
                </Typography>
                <TextField autoFocus id="id" label="ID"
                    variant="outlined" style={{ marginTop: '1rem', marginBottom: '1rem' }}
                    onChange={(e) => setNewEstado({ ...newEstado, ['id']: parseInt(e.target.value) })} />
                <TextField fullWidth id="nombre" label="Nombre del estado"
                    variant="outlined" style={{ marginBottom: '1rem' }}
                    onChange={(e) => setNewEstado({ ...newEstado, ['nombre']: e.target.value })} />
                <Button fullWidth variant="contained" disabled={!(newEstado.id && newEstado.nombre)}
                    onClick={() => saveOnRedux()}>
                    Guardar
                </Button>
            </Box>
        </Modal>
    );
}

export default AddEstado;