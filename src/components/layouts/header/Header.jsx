import React, { useState, useEffect } from 'react';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import { useSelector, useDispatch } from 'react-redux';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import AddEstado from './AddEstado';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import { resetEstados } from '../../../redux/actions/General.actions';

const Header = ({ title }) => {

    const { estados } = useSelector(state => state.General);
    const [open, setOpen] = useState(false);
    const [messageAlert, setMessageAlert] = useState(null);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const dispatch = useDispatch();

    useEffect(() => {
        const timeId = setTimeout(() => {
            setMessageAlert(null)
        }, 3000)
        return () => {
            clearTimeout(timeId)
        }
    }, [messageAlert]);

    return (
        <React.Fragment>
            <Toolbar sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Typography component="h2" variant="h5" color="inherit" align="center" noWrap sx={{ flex: 1 }}>
                    <Link href="/" color="inherit">{title}</Link>
                </Typography>
                <IconButton>
                    <SearchIcon />
                </IconButton>
            </Toolbar>
            <div className="grid grid-cols-3 gap-3">
                <div>
                    <h3>
                        Estados
                        <IconButton color="primary" aria-label="upload picture" component="label" onClick={handleOpen}>
                            <AddCircleOutlineIcon />
                        </IconButton>
                    </h3>
                </div>
                <div></div>
                <div align="right">
                    <Button size='small' variant="outlined" color="error" style={{ marginTop: '0.5rem' }}
                        onClick={() => dispatch(resetEstados())}>Reset Estados</Button>
                </div>
            </div>
            {messageAlert &&
                <Alert onClose={() => setMessageAlert(null)} style={{ marginTop: '0.2rem' }}>{messageAlert}</Alert>
            }
            <Toolbar
                component="nav" variant="dense"
                sx={{ justifyContent: 'center', overflowX: 'auto' }}>
                {estados.map((edo) => (
                    <Link
                        color="inherit"
                        noWrap
                        key={edo.nombre}
                        variant="body2"
                        href={`/listado/${edo.nombre}`}
                        sx={{ p: 1, flexShrink: 0 }}>
                        {edo.nombre}
                    </Link>
                ))}

            </Toolbar>

            <AddEstado open={open} setOpen={setOpen} handleClose={handleClose} setMessageAlert={setMessageAlert} />

        </React.Fragment >
    );
}
export default Header;