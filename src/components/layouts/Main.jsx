import React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

function Main() {
    const posts = [
        {name: "hola"}
    ];

    return (
        <Grid
            item
            xs={12}
            md={8}
            sx={{
                '& .markdown': {
                    py: 3,
                },
            }}>
            <Typography variant="h6" gutterBottom>
                {"Main"}
            </Typography>
            <Divider />
            {posts.map((post) => (
                <h1>{post.name}</h1>
            ))}
        </Grid>
    );
}


export default Main;
