import React from 'react';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

const Lugar = ({ lugar }) => {

    return (
        <React.Fragment>

            <Grid item key={lugar.nombre} xs={12} sm={6} md={4}>
                <Card>
                    <CardHeader
                        title={lugar.nombre}
                        titleTypographyProps={{ align: 'center' }}
                        sx={{
                            backgroundColor: (theme) =>
                                theme.palette.mode === 'light'
                                    ? theme.palette.grey[200]
                                    : theme.palette.grey[700],
                        }}
                    />
                    <CardContent>
                        <Box sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'baseline',
                            mb: 2,
                        }} >
                            <CardMedia
                                component="img"
                                sx={{
                                    pt: '0.5%',
                                    alignContent: 'center'
                                }}
                                image={lugar.image}
                                alt="random"
                            />
                        </Box>
                    </CardContent>
                    <Typography variant="p" color="text.primary">
                        {lugar.descripcion}
                    </Typography>
                </Card>
            </Grid>
        </React.Fragment>
    );
}
export default Lugar;