import React, { useEffect, useState } from 'react';
import Banner from './layouts/Banner';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import CardPost from './layouts/CardPost';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Listado = () => {
    const { posts } = useSelector(state => state.Post);
    const { estado } = useParams();
    const [listado, setListado] = useState([]);
    
    useEffect(() => {
        const getPosts = () => {
            const search = posts.filter(p => p.estado == estado);
            setListado(search);
        };
        const timeId = setTimeout(() => {
            getPosts()
          }, 1000)
          return () => {
            clearTimeout(timeId)
          }
    }, []);

    return (
        <React.Fragment>
            <Banner title={estado} size={"h3"} image={""} />
            <div className="row">
                <Typography variant="h4" gutterBottom style={{marginBottom: '1rem'}}>
                    Listado
                </Typography>
            </div>
            {listado.length > 0 ?
                <Grid container spacing={4}>
                    {listado.map((post) => (
                        <CardPost key={post.id} post={post} />
                    ))}
                </Grid>
                :
                <div className="grid grid-cols-4 gap-4">
                    <div></div>
                    <div></div>
                    <div>
                        <Box sx={{ display: 'flex' }}>
                            <CircularProgress />
                        </Box>
                    </div>
                </div>
            }
        </React.Fragment>
    );
}

export default Listado;